/* eslint-disable */
import React, { useEffect, useState, useRef, useCallback } from "react";
import CustomDropdown from "../../../Components/CustomDropdown";
import Graph from "./Graph";
import "./styles.scss";

const SOCKET_API_KEY =
  "df6a0e570da26b416fff8670ea093d901f30f968ceb08ccddde2a22fcb2f31f0";
const SOCKET_API_URL = "wss://streamer.cryptocompare.com/v2";

const COIN_OPTIONS = [
  { id: "BTC", value: "Bitcoin" },
  { id: "ETH", value: "Ether" },
  { id: "LTC", value: "Litecoin" },
  { id: "ZEC", value: "Zcash" },
  { id: "VTC", value: "Verticoin" },
  { id: "XLM", value: "Stellar Lumen" },
];

const AccuracyUp = () => {
  const socket = useRef(null);
  const componentHasUnmounted = useRef(false);
  const [dataSetOne, setDataSetOne] = useState([]);
  const [dataSetTwo, setDataSetTwo] = useState([]);
  const [dataSetOneCurrency, setDataSetOneCurrency] = useState(
    COIN_OPTIONS[0].id
  );
  const [dataSetTwoCurrency, setDataSetTwoCurrency] = useState(
    COIN_OPTIONS[1].id
  );
  const oldSubscription = useRef([COIN_OPTIONS[0].id, COIN_OPTIONS[1].id]);
  const incommingDataArrayRef = useRef({ firstSet: [], secondSet: [] }).current;

  useEffect(() => {
    socket.current = new WebSocket(
      `${SOCKET_API_URL}?api_key=${SOCKET_API_KEY}`
    );

    if (socket.current) {
      socket.current.addEventListener("open", handleOnSuccessfullConnection);
      socket.current.addEventListener("message", handleOnRecieveMessage);
      socket.current.addEventListener("close", handleOnCloseConnection);
    }

    return () => {
      if (socket.current) {
        componentHasUnmounted.current = true;
        socket.current.removeEventListener(
          "open",
          handleOnSuccessfullConnection
        );
        socket.current.removeEventListener("message", handleOnRecieveMessage);
        socket.current.close();
      }
    };
  }, []);

  const setNewSubscription = () => {
    return JSON.stringify({
      action: "SubAdd",
      subs: [
        `0~Coinbase~${dataSetOneCurrency}~USD`,
        `0~Coinbase~${dataSetTwoCurrency}~USD`,
      ],
    });
  };

  useEffect(() => {
    setNewSubscription();
  }, [dataSetOneCurrency, dataSetTwoCurrency]);

  const insertMaxTenArray = (arrayData, dataToInsert) => {
    arrayData.push(dataToInsert);

    if (arrayData.length > 10) {
      arrayData.shift();
    }

    return arrayData;
  };

  const handleOnRecieveMessage = (event) => {
    const incomingData = JSON.parse(event.data);
    console.log(incomingData);

    if (incomingData.M === "Coinbase") {
      if (incomingData.FSYM === dataSetOneCurrency) {
        incommingDataArrayRef.firstSet = insertMaxTenArray(
          incommingDataArrayRef.firstSet.slice(),
          incomingData?.P
        );

        setDataSetOne(incommingDataArrayRef.firstSet);
      } else {
        incommingDataArrayRef.secondSet = insertMaxTenArray(
          incommingDataArrayRef.secondSet.slice(),
          incomingData?.P
        );

        setDataSetTwo(incommingDataArrayRef.secondSet);
      }
    }
  };

  const handleOnSuccessfullConnection = () => {
    console.log("open");
    socket.current.send(setNewSubscription());
  };

  const removeSubscription = () => {
    return JSON.stringify({
      action: "SubRemove",
      subs: [
        `30~Coinbase~${oldSubscription.current[0]}~USD`,
        `30~Coinbase~${oldSubscription.current[1]}~USD`,
      ],
    });
  };

  const handleOnCloseConnection = () => {
    if (!componentHasUnmounted.current) {
      socket.current = new WebSocket(
        `${SOCKET_API_URL}?api_key=${SOCKET_API_KEY}`
      );
    } else {
      socket.current = null;
    }
  };

  const handleOnChangeFilter = useCallback(
    (data, type) => {
      if (type === "dataSetOne") {
        oldSubscription.current[0] = data;
        setDataSetOneCurrency(data);
      } else {
        oldSubscription.current[1] = data;
        setDataSetTwoCurrency(data);
      }

      removeSubscription();
    },
    [dataSetOneCurrency, dataSetTwoCurrency]
  );

  return (
    <section className="accuracy-sec-wrap">
      <div className="info-header-wrap">
        <div className="left-part">
          <img
            className="bitcoin-icon"
            src={require("../../../assets/img/bitcoin.svg").default}
            alt="bitcoin"
          />
          <span className="graph-name">Real-time bitcoin graph</span>
        </div>

        <div className="right-part">
          <div className="currency-options">
            <CustomDropdown
              primary
              secondary
              className="cur-sel cur-a"
              popClassName="cur-sel-dropdown"
              list={COIN_OPTIONS}
              value={dataSetOneCurrency}
              setChange={(data) => handleOnChangeFilter(data, "dataSetOne")}
            />
            <CustomDropdown
              primary
              className="cur-sel cur-b"
              popClassName="cur-sel-dropdown"
              list={COIN_OPTIONS}
              value={dataSetTwoCurrency}
              setChange={(data) => handleOnChangeFilter(data, "dataSetTwo")}
            />
          </div>
        </div>
      </div>

      <div className="graph-container">
        <Graph
          dataSetOne={dataSetOne}
          dataSetTwo={dataSetTwo}
          dataSetOneCurrency={dataSetOneCurrency}
          dataSetTwoCurrency={dataSetTwoCurrency}
        />
      </div>
    </section>
  );
};

export default AccuracyUp;
