import React from "react";
import { Popover, MenuItem } from "@material-ui/core";
import classNames from "classnames";

const CustomPopover = ({
  list,
  value,
  valueid,
  displayid,
  handleChange,
  setAnchorEl,
  anchorEl,
  primary,
  secondary,
  popClassName,
}) => {
  return (
    <Popover
      open={Boolean(anchorEl)}
      className={classNames("custom-dd-popover", popClassName, {
        primary: !!primary,
        secondary: !!secondary,
      })}
      anchorEl={anchorEl}
      onClose={() => setAnchorEl(null)}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "center",
      }}
      transformOrigin={{
        vertical: "top",
        horizontal: "center",
      }}
    >
      {list &&
        list.map((item) => (
          <MenuItem
            className={"dd-menu-item" + (item[valueid] === value ? " active" : "")}
            key={item[valueid]}
            value={item[valueid]}
            onClick={() => handleChange(item)}
          >
            {item[displayid]}
          </MenuItem>
        ))}
    </Popover>
  );
};
export default CustomPopover;
