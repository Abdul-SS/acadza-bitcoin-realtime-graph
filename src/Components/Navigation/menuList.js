export const menuList = [
  {
    name: 'Dashboard',
    path: '/Dashboard',
    icon: require('../../assets/img/dashboard.svg').default
  },
  {
    name: 'Backlog Remover',
    path: '/BacklogRemover',
    icon: require('../../assets/img/backlog.svg').default
  },
  {
    name: 'Rank up',
    path: '/RankUp',
    icon: require('../../assets/img/rank.svg').default
  },
  {
    name: 'Speed up',
    path: '/SpeedUp',
    icon: require('../../assets/img/speed.svg').default
  },
  {
    name: 'Accuracy up',
    path: '/AccuracyUp',
    icon: require('../../assets/img/accuracy.svg').default
  },
  {
    name: 'Revision',
    path: '/Revision',
    icon: require('../../assets/img/revision.svg').default
  },
  {
    name: 'Test Creator',
    path: '/TestCreator',
    icon: require('../../assets/img/test.svg').default
  },
  {
    name: 'Assignment Creator',
    path: '/AssignmentCreator',
    icon: require('../../assets/img/assignment.svg').default
  },
  {
    name: 'Study Material',
    path: '/StudyMaterial',
    icon: require('../../assets/img/study.svg').default
  },
  {
    name: 'Formula Sheet',
    path: '/FormulaSheet',
    icon: require('../../assets/img/rank.svg').default
  }
]
