import React, { useState } from "react";
import { ArrowDropDown } from "@material-ui/icons";
import { IconButton } from "@material-ui/core";
import classNames from "classnames";

import CustomPopover from "../CustomPopover";
import "./style.scss";

const IconMenu = ({
  name,
  icon,
  tooltip,
  showArrow,
  display,
  value,
  valueId,
  list,
  className,
  popClassName,
  onChange,
  onSelect,
  primary,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const valueid = !!valueId ? valueId : "id";
  const displayid = !!display ? display : "value";

  const changeValue = (value) => {
    setAnchorEl(null);
    !!onChange &&
      onChange({
        target: {
          name,
          value,
        },
      });
    !!onSelect && onSelect(value);
  };

  const handleChange = (data) => {
    if (!!data?.onClick) data.onClick();
    else if (!!onChange || !!onSelect) changeValue(data[valueid]);
  };

  return (
    <>
      <span
        className={classNames("icon-dropdown", className, {
          "text-dd-open": Boolean(anchorEl),
          "text-dd-empty": !value,
          primary: !!primary,
        })}
        onClick={(e) => setAnchorEl(e.currentTarget)}
      >
        <IconButton className="dd-menu-icon" title={tooltip}>
          {icon}
        </IconButton>
        {showArrow && (
          <span className="dd-icon">
            <ArrowDropDown />
          </span>
        )}
      </span>
      <CustomPopover
        list={list}
        value={value}
        valueid={valueid}
        displayid={displayid}
        handleChange={handleChange}
        setAnchorEl={setAnchorEl}
        anchorEl={anchorEl}
        popClassName={popClassName}
      />
    </>
  );
};

export default IconMenu;
