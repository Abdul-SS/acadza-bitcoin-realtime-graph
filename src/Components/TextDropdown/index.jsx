import React, { useState } from "react";
import { ArrowDropDown } from "@material-ui/icons";
import CustomPopover from "../CustomPopover";
import classNames from "classNames";
import "./style.scss";

const TextDropdown = ({
  name,
  display,
  value,
  valueId,
  list,
  className,
  popClassName,
  onChange,
  onSelect,
  primary,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const valueid = !!valueId ? valueId : "id";
  const displayid = !!display ? display : "value";

  const sortName = (id) => {
    const valueFilter = list?.filter((i) => i[valueid] === id);
    return valueFilter?.length > 0 ? valueFilter[0]?.[displayid] : "";
  };

  const handleChange = (value) => {
    setAnchorEl(null);
    !!onChange &&
      onChange({
        target: {
          name,
          value,
        },
      });
    !!onSelect && onSelect(value);
  };

  return (
    <>
      <span
        className={classNames("text-dropdown", className, {
          "text-dd-open": Boolean(anchorEl),
          "text-dd-empty": !value,
          primary: !!primary,
        })}
        onClick={(e) => setAnchorEl(e.currentTarget)}
      >
        <span className="dd-value">{sortName(value)}</span>
        <span className="dd-icon">
          <ArrowDropDown />
        </span>
      </span>
      <CustomPopover
        list={list}
        value={value}
        valueid={valueid}
        displayid={displayid}
        handleChange={handleChange}
        setAnchorEl={setAnchorEl}
        anchorEl={anchorEl}
        primary={primary}
        secondary={secondary}
        popClassName={popClassName}
      />
    </>
  );
};

export default TextDropdown;
