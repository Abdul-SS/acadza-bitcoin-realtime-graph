import React, { useState } from "react";
import { ArrowDropDown } from "@material-ui/icons";
import classNames from "classnames";

import CustomPopover from "../CustomPopover";
import "./style.scss";

const CustomDropdown = ({
  display,
  value,
  valueId,
  list,
  className,
  popClassName,
  setChange,
  primary,
  secondary,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const valueid = valueId || "id";
  const displayid = display || "value";

  const sortName = (id) => {
    const valueFilter = list?.filter((item) => item[valueid] === id);

    return valueFilter?.length > 0 ? valueFilter[0]?.[displayid] : "";
  };

  const handleChange = (data) => {
    setAnchorEl(null);

    if (setChange) {
      setChange(data[valueid]);
    }
  };

  return (
    <>
      <div
        className={classNames("custom-dropdown", className, {
          "custom-dd-open": Boolean(anchorEl),
          "custom-dd-empty": !value,
          primary: !!primary,
          secondary: !!secondary,
        })}
        onClick={(event) => setAnchorEl(event.currentTarget)}
      >
        <span className="dropdown-content">
          <span className="dropdown-selected">{sortName(value)}</span>
          <span className="dd-icon">
            <ArrowDropDown />
          </span>
        </span>
      </div>
      <CustomPopover
        list={list}
        value={value}
        valueid={valueid}
        displayid={displayid}
        handleChange={handleChange}
        setAnchorEl={setAnchorEl}
        anchorEl={anchorEl}
        primary={primary}
        secondary={secondary}
        popClassName={popClassName}
      />
    </>
  );
};

export default CustomDropdown;
