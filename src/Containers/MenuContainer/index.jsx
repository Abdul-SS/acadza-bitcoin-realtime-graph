import React, { useState } from "react";
import { Switch, Route, Redirect, useHistory } from "react-router-dom";
import { Container, Grid } from "@material-ui/core";
import classNames from "classnames";

import Header from "../../Components/Header";
import "./styles.scss";
import Navigation from "../../Components/Navigation";
import AccuracyUp from "../Screens/AccuracyUp";
import DefaultContainer from "../Screens/DefaultContainer";

const MenuContainer = () => {
  const history = useHistory();
  const [isMenu, setMenu] = useState(false);
  const [selectedNavBar, setSelectedNavBar] = useState("Accuracy up");

  const handleOnSelect = (data) => {
    setSelectedNavBar(data.name);
    history.push(data.path);
  };

  return (
    <div className="home-container">
      <Header isMenu={isMenu} setMenu={setMenu} />
      <Grid container className="home-grid-wrap">
        <Grid
          item
          xs={12}
          lg={2}
          className={classNames("navigation-grid", isMenu && "menu-open")}
        >
          <Navigation
            isMenu={isMenu}
            setMenu={setMenu}
            selectedNavBar={selectedNavBar}
            handleOnSelect={handleOnSelect}
          />
        </Grid>
        <Grid item xs={12} lg={10} className="pages-grid">
          <Container maxWidth="xl" className="pages-container">
            <Switch>
              <Redirect exact from="/" to="/AccuracyUp" />
              <Route exact path="/AccuracyUp">
                <AccuracyUp />
              </Route>
              <Route>
                <DefaultContainer tabName={selectedNavBar} />
              </Route>
            </Switch>
          </Container>
        </Grid>
      </Grid>
    </div>
  );
};

export default MenuContainer;
