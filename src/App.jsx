/* eslint-disable react-hooks/exhaustive-deps */

import React from "react";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core";
import MenuContainer from "./Containers/MenuContainer";

// Setting Theme for an application
export const DEFAULT_THEME = createMuiTheme({
  palette: {
    primary: {
      main: "#DC004E",
    },
    secondary: {
      main: "#1976D2",
    },
  },
});

const App = () => {
  return (
    <MuiThemeProvider theme={DEFAULT_THEME}>
      <MenuContainer />
    </MuiThemeProvider>
  );
};

export default App;
