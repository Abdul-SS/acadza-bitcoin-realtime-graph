import React from "react";
import "./styles.scss";

const DefaultContainer = ({ tabName }) => {
  return <div className="default-page-root-container">{tabName}</div>;
};

export default DefaultContainer;
